<meta charset="utf-8">

<?php

include "spajanje_na_bazu.php";
	
	function vratiSedamRandomPitanja($korisnikID, $veza){
		$upit1 = "SELECT * FROM korisnikzadatak WHERE korisnikzadatak.userID = ".$korisnikID." AND korisnikzadatak.tocno = 0".
			" AND korisnikzadatak.zadnjePitan IS NULL ORDER BY RAND() LIMIT 7;";
		$upit2 = "SELECT * FROM korisnikzadatak WHERE userID = ".$korisnikID." AND tocno = 1 ORDER BY zadnjePitan LIMIT 7;";
		$upit3 = "SELECT * FROM korisnikzadatak WHERE userID = ".$korisnikID." AND tocno = 0 AND zadnjePitan IS NOT NULL ORDER BY RAND() LIMIT 7;";
		$neodgovarana = mysqli_query($veza,$upit1 ) or die ("Neispravan upit: " . $upit1);
		$stara = mysqli_query($veza,$upit2 ) or die ("Neispravan upit: " . $upit2);
		$kriva = mysqli_query($veza,$upit3 ) or die ("Neispravan upit: " . $upit3);
		
		$ukupno = 0;
		$brojac = 0;
		$pitanja = [];
		
		while($ukupno < 7 && $brojac < 5 && $redak = mysqli_fetch_array ($neodgovarana, MYSQLI_ASSOC)) {
			//$redak = mysqli_fetch_array ($neodgovarana, MYSQLI_ASSOC);
			$pitanja[$ukupno] = $redak;
			$ukupno++;
			$brojac++;
		}
		while($ukupno < 7 && $redak = mysqli_fetch_array ($kriva, MYSQLI_ASSOC)){
			$pitanja[$ukupno] = $redak;
			$ukupno++;
		}
		while($ukupno < 7 && $redak = mysqli_fetch_array ($stara, MYSQLI_ASSOC)){
			$pitanja[$ukupno] = $redak;
			$ukupno++;
		}
				//var_dump($pitanja);
		while($ukupno < 7 && $redak = mysqli_fetch_array ($neodgovarana, MYSQLI_ASSOC)){
			$pitanja[$ukupno] = $redak;
			$ukupno++;
		}
				//var_dump($pitanja);
		//zakljucak je da negdje mora biti dovoljno pitanja za generiranje
		//najprije dodajem 5 neodgovorenih, onda koliko je moguce krivih, onda stara, a ako nije popunjeno 7 pitanja
		//onda se dodaje ostatak neodgovorenih (situacija da korisnik otpocetka krece s random generacijom)
	
		return $pitanja;
	}
	
	function vratiPitanjaKategorije($kategorijaID, $korisnikID, $veza){
		$upit = "SELECT * FROM zadatak WHERE rbrKat = ".$kategorijaID.";";
		$pitanja;
		$ukupno = 0;
		$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
		while($redak = mysqli_fetch_array ($rezultat, MYSQLI_ASSOC)){
			$pitanja[$ukupno] = $redak;
			$ukupno++;
		}
		
		
		return $pitanja;	
	}
	
	function provjeriTocnost($pitanje, $odgovor){
		if(strtoupper(trim($pitanje['odgovor']))==strtoupper($odgovor)){
			return 1;
		}
		else{
			return 0;
		}
	} 
	
	function odgovaranje($pitanja, $odgovori, $korisnik, $brojPitanja, $veza){
		$i = 0; $brTocnih = 0;
		while(($i+1) < $brojPitanja){
			echo '<div class = "well">';
			//$pitanja[$i] = trim($pitanja[$i]['odgovor']);
			$pitanja[$i]['novo'] = provjeriTocnost($pitanja[$i], $odgovori[$i]);
			echo ($i+1).'.) '.$pitanja[$i]['pitanje'];
			echo '<br>';
			if($pitanja[$i]['novo'] == 0){
				echo '<p class="bg-danger">';
				echo 'Tocan odgovor: '.$pitanja[$i]['odgovor'];
				echo '<br>';
				echo 'Vas odgovor: '.$odgovori[$i];
				echo '<br>';
				echo '<br>';
				echo '</p>';
			}
			else{
				echo '<p class="bg-success">';
				echo "TOCNO STE ODGOVORILI: ".$pitanja[$i]['odgovor'];
				echo '<br>';
				echo '<br>';
				echo '</p>';
				$brTocnih++;
			}
			$i++;
			echo '</div>';
		}
		echo '<div class = "well"><h3>';
		if($brTocnih >= ($brojPitanja-1)/2){
			echo '<p class="bg-success">';
			if($brTocnih == ($brojPitanja-1)){
				echo 'SVAKA ČAST, TOČNO STE ODGOVORILI NA SVA PONUĐENA PITANJA';
				//
				echo '<br><mark>REZULTAT: '.$brTocnih.'/'.($brojPitanja-1).'</mark>';
				
			}
			else{
				echo 'ČESTITAMO, IMATE VIŠE OD 50% TOČNIH ODGOVORA';
				echo '<br><mark>REZULTAT: '.$brTocnih.'/'.($brojPitanja-1).'</mark>';
			}
		}
		else{
			echo '<p class="bg-danger">';
			echo 'NAŽALOST IMATE MANJE OD 50% TOČNIH ODGOVORA.';
			echo '<br><mark>REZULTAT: '.$brTocnih.'/'.($brojPitanja-1).'</mark>';
		}
		echo '</p>';
		echo '</div></h3>';
		$i = 0;
		while(($i+1) < $brojPitanja){
			$upit = "SELECT * FROM korisnikzadatak WHERE userID = ".$korisnik." AND rbrZad = ".$pitanja[$i]['rbrZad'].";";
			$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
			$redak = mysqli_fetch_array ($rezultat, MYSQLI_ASSOC);
			
			if($pitanja[$i]['novo'] == $redak['tocno']){
				$upit = "UPDATE korisnikzadatak SET zadnjePitan = '".gmdate("Y-m-d H:i:s")."' WHERE userID = ".$korisnik.
				" AND rbrZad = ".$pitanja[$i]['rbrZad'].";";
				mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
			}
			else{
				$upit = "UPDATE korisnikzadatak SET zadnjePitan = '".gmdate("Y-m-d H:i:s")."', tocno = ".$pitanja[$i]['novo']." WHERE userID = ".
				$korisnik." AND rbrZad = ".$pitanja[$i]['rbrZad'].";";
				mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
				//$upit = "SELECT zadatak.rbrKat FROM zadatak WHERE zadatak.rbrZad = ".$pitanja[$i]['rbrZad'];
				//$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
				//$rezultat = mysqli_fetch_array ($rezultat, MYSQLI_ASSOC);
				$znak;
				if($pitanja[$i]['novo'] == 1){
					$znak = '+';
				}
				else{
					$znak = '-';
				}
				$upit = "UPDATE korisnikkategorija SET brTocnih = brTocnih ".$znak." 1 WHERE userID = ".$korisnik.
					" AND rbrKat = ".$pitanja[$i]['rbrKat'];
				mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
				
				//update koncepta
				
				$upit = "SELECT * FROM zadatakkoncept WHERE zadatakkoncept.rbrZad = ".$pitanja[$i]['rbrZad'].";";
				$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
				while($redak = mysqli_fetch_array ($rezultat, MYSQLI_ASSOC)){
					$upit = "UPDATE korisnikkoncept SET brBodova = brBodova ".$znak.$redak['brBodova']." WHERE userID = ".$korisnik.
						" AND rbrKon = ".$redak['rbrKon']; 
						//echo $upit;
					mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
				}
			}
			$i++;
		}
		otvoriNoveKategorije($korisnik, $veza);
		otvoriNoveKoncepte($korisnik, $veza);
	}
	
	function otvoriNoveKategorije($korisnik, $veza){
		$upit = "SELECT * FROM korisnikkategorija WHERE userID = ".$korisnik;
		$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
		while($redak = mysqli_fetch_array ($rezultat, MYSQLI_ASSOC)){
			$upit = "SELECT * FROM kategorija WHERE rbrKat = ".$redak['rbrKat'];
			$rez = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
			$pom = mysqli_fetch_array ($rez, MYSQLI_ASSOC);
			if($redak['brTocnih'] >= $pom['prag'] && $redak['otvorena'] == 0)
			{
				$upit = "UPDATE korisnikkategorija SET otvorena = -1 WHERE userID = ".$korisnik." AND rbrKat = ".$redak['rbrKat'].";";
				//echo $upit.'<br>';
				mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
			}
		}
		$upit = "SELECT * FROM kategorija";
		$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
				//$rez = vratiSveKategorijaKorisnika($korisnik,$veza);
				//var_dump($rez); 
		while($redak = mysqli_fetch_array($rezultat, MYSQLI_ASSOC)){
			$trenutna = $redak['rbrKat'];
			$upit = "SELECT COUNT(*) AS ispis FROM kategorijakategorija WHERE rbrPodKat = ".$trenutna.";";
			$broj1 = vratiBroj('ispis', $veza, $upit);
			$upit = "SELECT COUNT(korisnikkategorija.rbrKat) AS ispis FROM korisnikkategorija JOIN kategorijakategorija ON korisnikkategorija.rbrKat = kategorijakategorija.rbrNadKat JOIN kategorija ON kategorijakategorija.rbrNadKat = kategorija.rbrKat WHERE brTocnih >= prag AND kategorijakategorija.rbrPodKat = ".$trenutna." AND korisnikkategorija.userID = ".$korisnik.";";
			$broj2 = vratiBroj('ispis', $veza, $upit);
			$upit = "UPDATE korisnikkategorija SET otvorena = -1 WHERE ".$broj1." = ".$broj2." AND korisnikkategorija.userID = ".$korisnik." AND korisnikkategorija.otvorena = 0 AND korisnikkategorija.rbrKat = ".$trenutna.";";
							//echo $upit.'<br>';
			mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
		}
		
		//otvori pitanja
		$upit = "SELECT zadatak.rbrZad FROM zadatak JOIN korisnikkategorija ON zadatak.rbrKat = korisnikkategorija.rbrKat WHERE korisnikkategorija.otvorena = -1 AND korisnikkategorija.userID = ".$korisnik.";";
						//echo $upit.'<br>';
		$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
		while($redak = mysqli_fetch_array($rezultat, MYSQLI_ASSOC)){
			$upit = "INSERT INTO korisnikzadatak (userID, rbrZad, tocno) VALUES (".$korisnik.",".$redak['rbrZad'].", 0)";		
			mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
		}
					//$rez = vratiSveKategorijaKorisnika($korisnik,$veza);
				//var_dump($rez); 
		
		$upit = "UPDATE korisnikkategorija SET otvorena = 1 WHERE korisnikkategorija.otvorena = -1 AND korisnikkategorija.userID =".$korisnik.";";
						//echo $upit.'<br>';
		mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
	}
	
	function otvoriNoveKoncepte($korisnik, $veza){
		$upit = "SELECT DISTINCT rbrKon FROM zadatakkoncept JOIN korisnikzadatak ON zadatakkoncept.rbrZad = korisnikzadatak.rbrZad WHERE korisnikzadatak.userID = ".$korisnik.";";
		$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
		while($redak = mysqli_fetch_array($rezultat, MYSQLI_ASSOC)){
			$upit = "UPDATE korisnikkoncept SET otvoren = 1 WHERE korisnikkoncept.userID =".$korisnik." AND korisnikkoncept.rbrKon = ".$redak['rbrKon'].";";
			mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
		}
	}
	
	function vratiBroj($param, $veza, $upit){
		$rezultat = mysqli_query($veza,$upit ) or die ("Neispravan upit: " . $upit);
		$redak = mysqli_fetch_array ($rezultat, MYSQLI_ASSOC);
		return $redak[$param];
	}
	
	function PretvorbaIDPitanje($pitanja, $veza){
		$pom = [];
		$i = 0;
		$rezultat = [];
		while($i < 7){
			$pom[$i] = $pitanja[$i]['rbrZad'];
			$i++;
		}
		$dio = implode(', ', $pom);
		$dio = '('.$dio.')';
		//var_dump($dio);
		$upit = "SELECT * FROM zadatak WHERE rbrZad  IN ".$dio.";";
		//echo $upit;
		$rezultat = mysqli_query($veza,$upit) or die ("Neispravan upit: " . $upit);	
		$pitanja; $i = 0;
		while($redak = mysqli_fetch_array($rezultat, MYSQLI_ASSOC)){
			$pitanja[$i] = $redak;
			$i++;
		}
		return $pitanja;
	}
	
	function vratiKoncepteKorisnika($korisnik, $veza){
		$upit = "SELECT * FROM korisnikkoncept JOIN koncept ON korisnikkoncept.rbrKon = koncept.rbrKon WHERE korisnikkoncept.userID = ".$korisnik." ;";
		$rezultat = mysqli_query($veza,$upit) or die ("Neispravan upit: " . $upit);
		$povratna;
		$i = 0;
		while($redak = mysqli_fetch_array($rezultat, MYSQLI_ASSOC)){
			//var_dump($redak);
			$povratna[$i] = $redak;
			$i++;
		}
		return $povratna;
	}
	//malo sam dodala koda t.d. mi je fja upotrebljiva za printanje otvorenih kategorija 
	function vratiKategorijaKorisnika($korisnik,$veza){
		$upit = "SELECT * FROM korisnikkategorija join kategorija ON korisnikkategorija.rbrKat = kategorija.rbrKat WHERE korisnikkategorija.userID = ".$korisnik." AND otvorena = 1;";
				$rezultat = mysqli_query($veza,$upit) or die ("Neispravan upit: " . $upit);
		$povratna[] = 0;
		$i = 0;
		while($redak = mysqli_fetch_array($rezultat, MYSQLI_ASSOC)){
			$povratna[$i] = $redak;
			$i++;
		}
		return $povratna;
	}
	
		function vratiSveKategorijaKorisnika($korisnik,$veza){
		$upit = "SELECT * FROM korisnikkategorija join kategorija ON korisnikkategorija.rbrKat = kategorija.rbrKat WHERE korisnikkategorija.userID = ".$korisnik.";";
				$rezultat = mysqli_query($veza,$upit) or die ("Neispravan upit: " . $upit);
		$povratna[] = 0;
		$i = 0;
		while($redak = mysqli_fetch_array($rezultat, MYSQLI_ASSOC)){
			$povratna[$i] = $redak;
			$i++;
		}
		return $povratna;
	}
	 
	function vratiUserID ($username,$veza) {
		$upit = "SELECT userID from korisnik where username = ".$username.";";
		$rezultat = mysqli_query($veza,$upit) or die ("Neispravan upit: " . $upit);
		$row = mysqli_fetch_array($rezultat);
	  	$pomID = $row["userID"];
		return $pomID;
		
	}
	
	function checkIfEmpty($val, $default) {
		return empty($val) ? $default : $val;
	}
	
	function _get($name, $default = NULL) {
		return isset($_GET[$name]) ? $_GET[$name] : $default;
	}
	
	
?>