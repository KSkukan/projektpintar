<?php
require_once "header.php"

?>

<!DOCTYPE html>

<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Matematika 3R</title>
	<meta name="description" content="Hello World">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
</head>

<body>
<style>
.grey {
		background-color: #76CEE4;

}
</style>

<div class = "grey">
	<div class = "container">
	
		
			<center>
			<!-- <h1><span style="color:#66FF33"> Uspješna prijava, <?php echo  $_SESSION['MM_Username'] ?> ! </span> </h1> -->
 <br/>
 			<div class = "well"	>

 				<d1>
				 	<dt>Vaše otvorene kategorije su:</dt>
				 	<?php
				 	echo '<h3>';
				 	require_once "algoritmi.php";
				 	require_once "spajanje_na_bazu.php";
				 
				 	$username = GetSQLValueString($_SESSION['MM_Username'], "text");	
					$korisnikID = vratiUserID ($username,$veza);
					$_SESSION['MM_UsernameID'] = $korisnikID;
					
				 	$kategorije = vratiKategorijaKorisnika(GetSQLValueString($korisnikID, "int"),$veza);
					
					$i = 0;
					foreach ($kategorije as $kategorija){
						$value2 = $kategorija['rbrKat'];
				    	$i++; ?>
				    	
				    	<dd>
					    	<?=$i?>. <!-- isto kao <?php echo $i ?> -->

							<a href="pitanja.php?param1=1&param2=<?=$value2?>"><?=$kategorija['imeKat']?></a>
						</dd>
				<?php } ?>	

				</d1>
                	<br>
					<p><a href="pitanja.php?param1=0&param2=0">Želim algoritamski generirana pitanja</a></p>
                    <br>
					<p><a href="koncept.php">Pregled mojih koncepata</a></p>
				<br/>
				<br/>


			<br/>
			<br/>
            <?php echo '</h3>'; ?>
			<a href="<?php echo $logoutAction ?>">Odjava</a>

			</div>
	</div>
    
</div>
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</center>

</body>
</html>