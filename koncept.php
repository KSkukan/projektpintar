<?php 
require_once "header.php";
require_once "algoritmi.php";
require_once "spajanje_na_bazu.php";
?>


<!DOCTYPE <html lang="en">

<head>
	<meta charset="utf-8">
	<title>Koncepti korisnika</title>
	<meta name="description" content="Hello World">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	
</head>

<body>

<style>
.grey {
	background-color: #76CEE4;

}
</style>
	<center>
	<div class = "grey">
	<div class = "container">
    	<?php
			$korisnik = $_SESSION['MM_UsernameID'];
			$koncepti =  vratiKoncepteKorisnika($korisnik, $veza);
		?>
        <h3>
        	Vaši otvoreni koncepti su: 
           </h3>
        <?php
			foreach($koncepti as $koncept){
				//var_dump($koncept);
				
				if($koncept['otvoren'] == 1){
					echo '<div class = "well">';
					echo '<table width="532" border="0" bordercolor="#FFFFFF"><tr><td width="50%" height="145">';
					echo '<img src="./koncepti/'.$koncept['imeKon'].'2.png" width="266" height="145" /></td>';
					echo '<td width="50%"><div align="center">';
					echo $koncept['imeKon'];
					echo '<br>Ukupni broj bodova: '.$koncept['ukBod'];
					echo '<br>Prag koncepta je: '.$koncept['prag'];
					echo '<br>Vaš broj bodova je: '.$koncept['brBodova'];
					if($koncept['brBodova'] > $koncept['prag'])
					{
						if($koncept['brBodova'] == $koncept['prag']){
							echo '<br>ČESTITAMO!!!<br>RIJEŠILI STE CIJELI KONCEPT';
						}
						else{
							echo '<br>ČESTITAMO!!!<br>ZADOVOLJILI STE PRAG 50%';
						}
					}
					echo '</div></td></tr></table>';
					echo '<br>';
				}
				echo '</div>';
			}
		?>
		</div>
		</div>
	</center>
<p> Povratak na početnu <a href="index.php">stranicu.</a> </p>
 <a href="<?php echo $logoutAction ?>">Odjava</a> 
</body>
</html>
