<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Oops!</title>
	<meta name="description" content="Hello World">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	

</head>

<style>
.grey {
		background-color: #FF3F58;

}
</style>

<body>

	<div class = "grey">
		<div class = "container">

			<div class = "well"	>
				<h1><span style="color:#FF0000">Neuspješna prijava!</span></h1>
				Natrag na <a href="prijava.php">prijavu</a>.

			</div>

		</div>

	</div>

	
<br/>
    
 	<!-- Latest compiled and minified JavaScript -->
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
