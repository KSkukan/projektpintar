<!--
require je slicno kao include
-->
<?php require_once('Connections/User_Information.php'); ?>


<?php
//funkcije za "otkljucavanje" inicijalnih kategorija i koncepata za novoregistrirane korisnike

function insertKorisnikKategorija($id, $rbrKat, $brTocnih, $otvorena, $User_Information)
{
	$insertSQL = sprintf("INSERT INTO korisnikkategorija (userID, rbrKat, brTocnih, otvorena) VALUES (%s, %s, %s, %s)", $id, $rbrKat, $brTocnih, $otvorena);
	$Result = mysql_query($insertSQL, $User_Information) or die(mysql_error());
	
}

function insertKorisnikKoncept($id, $rbrKon, $brBodova, $otvoren, $User_Information)
{
	$insertSQL = sprintf("INSERT INTO korisnikkoncept (userID, rbrKon, brBodova, otvoren) VALUES (%s, %s, %s, %s)", $id, $rbrKon, $brBodova, $otvoren);
	$Result = mysql_query($insertSQL, $User_Information) or die(mysql_error());
	
}

function insertKorisnikZadatak($userID, $rbrZad,$User_Information) {
	$insertSQL = sprintf("INSERT INTO korisnikzadatak (userID, rbrZad, tocno) VALUES (%s, %s, 0)",  $userID, $rbrZad);
	$Result = mysql_query($insertSQL, $User_Information) or die(mysql_error());
}
?>


<?php
// funkcija koja formatira vrijednosti za komunikaciju s bazom
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
	{
	  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
	
	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
	
	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;    
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "defined":
		  $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
		  break;
	  }
	  return $theValue;
	}
}

//nesto za funkcionalnost formulara za registraciju
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

//upit za unos novog korisnika u DB
if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "registration")) {
  $insertSQL = sprintf("INSERT INTO korisnik (username, password) VALUES (%s, %s)",
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['password'], "text"));
					   
  //odabir baze
  mysql_select_db($database_User_Information, $User_Information);
  
  //upit koji dohvaca username iz baze za provjeru dali uneseni username vec postoji u bazi da se sprijece korisnici sa vise istih username-ova
  $testQuery = sprintf("SELECT username FROM korisnik WHERE username = %s", GetSQLValueString($_POST['username'], "text"));
  $testResult = mysql_query($testQuery, $User_Information) or die(mysql_error());
  
  if (mysql_num_rows($testResult) == 0){
  
  	  //ako je broj redova 0, znaci da taj username jos ne postoji u bazi i takav korisnik se smije upisat u bazu
	  
	  //unesi korisnika
	  $Result1 = mysql_query($insertSQL, $User_Information) or die(mysql_error());
	  
	  //dohvati userID od tog korisnika kako bi inicijalizirao tablice korisnikkategorija i korisnikkoncept
	  $tempSQL = sprintf("SELECT userID FROM korisnik WHERE username = %s", GetSQLValueString($_POST['username'], "text"));
	  $tempID = mysql_query($tempSQL, $User_Information) or die(mysql_error());
	  $row = mysql_fetch_array($tempID);
	  $pomID = $row["userID"];
	  
	  //petlja za unos inicijalnih vrijednosti u tablicu korisnikkategorija
	  for($i=1; $i<=9; $i++){
	  	
		if ($i == 1) insertKorisnikKategorija($pomID, $i, 0, 1, $User_Information);
		else insertKorisnikKategorija($pomID, $i, 0, 0, $User_Information);
		
	  }
	  
	  //petlja za unos inicijalnih vrijednosti u tablicu korisnikkoncept
	   for($i=1; $i<=4; $i++){
	  	
		if ($i == 1) insertKorisnikKoncept($pomID, $i, 0, 1, $User_Information);
		else insertKorisnikKoncept($pomID, $i, 0, 0, $User_Information);
		
	  }
	  
	   
	  //petlja za unos inicijalnih vrijednosti u tablicu korisnikzadatak
	  
	  $upit = "SELECT * FROM zadatak WHERE rbrKat = 1;";
	  $zadaci = mysql_query($upit,$User_Information) or die(mysql_error());
	  $temp = mysql_num_rows($zadaci);
	  $i = 0;
	  while ($i < $temp) {
	  	$red = mysql_fetch_array($zadaci);
	  	insertKorisnikZadatak($pomID, $red['rbrZad'],$User_Information);
	  	$i++;
	  }

	/*  foreach ($zadaci as $zadatak) {
	  	 $red = mysql_fetch_array($zadaci);
		 echo $red['rbrZad'];
	  	 insertKorisnikZadatak($pomID, $red['rbrZad'],$User_Information);
		
	  	}
	*/
	  //nakon upisivanja u bazu, usmjeri korisnika na prijavu
	  $insertGoTo = "prijava.php";
	  if (isset($_SERVER['QUERY_STRING'])) {
		$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
		$insertGoTo .= $_SERVER['QUERY_STRING'];
	  }
	  header(sprintf("Location: %s", $insertGoTo));
	  
  }
  
  //korisnik s takvim username-om vec postoji!
  else echo '<span style="color:#FF0000">Odabrano korisničko ime već postoji! Pokušajte ponovo. </span>';
}

//koristit ce se kasnije (na dnu datoteke) za oslobadanje resursa
mysql_select_db($database_User_Information, $User_Information);
$query_User_Request = "SELECT * FROM korisnik";
$User_Request = mysql_query($query_User_Request, $User_Information) or die(mysql_error());
$row_User_Request = mysql_fetch_assoc($User_Request);
$totalRows_User_Request = mysql_num_rows($User_Request);
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Registracija</title>
	<meta name="description" content="Hello World">
	
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">


</head>

<style>
.grey {
		background-color: #75E880;

}
</style>

<body>

<div class = "grey">
	<div class = "container">
		<h1> Registracija </h1>
	<div class = "well"	>
		<form method="POST" action="<?php echo $editFormAction; ?>"  name="registration">
		<!--	<div class = "row" > -->
				<label>Korisničko ime:<br/></label> 
				
				<input name="username" type="text" required="required"><br/>
			

		
				<label>Lozinka:<br/></label>
				<input name="password" type="password" required="required"><br/>
			<!-- </div> -->

			<button type="submit" class="btn btn-primary">Registracija</button>
			<input type="hidden" name="MM_insert" value="registration">
			
		</form>
	</div>
	</div
</div>

</form>

Već imate korisnički račun?  <a href="prijava.php">Prijava!</a>
</body>
</html>

<?php
mysql_free_result($User_Request);
?>